import { Game, Scene, WEBGL } from "phaser";
import "phaser/plugins/spine/dist/SpinePlugin";
import { bgFile, initRun, params } from "src/query";
import "./style.css";

initRun(run);

function run(spineV: string, animNum: number = 1, spineName: string) {
    new Game({
        type: WEBGL,
        scale: {
            width: parseInt(params.width),
            height: parseInt(params.height),
            autoCenter: 0,
            autoRound: false,
            mode: 0,
            resizeInterval: 80,
        },
        plugins: {
            scene: [
                {
                    key: "SpinePlugin",
                    plugin: window.SpinePlugin,
                    mapping: "spine",
                    start: true,
                },
            ],
        },
        scene: [getBoot(spineV, animNum, spineName)],
        seed: [(Date.now() * Math.random()).toString()],
        disableContextMenu: true,
        autoFocus: true,
        clearBeforeRender: false,
        render: {
            mipmapFilter: "LINEAR_MIPMAP_LINEAR",
            clearBeforeRender: false,
            failIfMajorPerformanceCaveat: true,
            roundPixels: false,
            pixelArt: false,
            antialias: true,
            antialiasGL: true,
            transparent: true,
            batchSize: 1024,
            premultipliedAlpha: false,
        },
    });
}

function getBoot(spineV: string, animNum: number, spineName: string) {
    return class Boot extends Scene {
        constructor() {
            super({ key: "boot" });
        }

        preload() {
            this.load.image(bgFile, "./assets/" + bgFile + ".png");
            this.load.spine("sp1", `./assets/${spineV}/${spineName}.json`, `./assets/${spineV}/${spineName}.atlas`);
        }

        create() {
            this.add.image(0, 0, bgFile).setOrigin(0, 0);
            this.add.image(0, 600, bgFile).setOrigin(0, 1).setScale(1, -1);

            const sp = this.add.spine(400, 300, "sp1");

            sp.setAnimation(0, sp.getAnimationList()[animNum], true);
        }
    };
}
