export const bgFile = "temple1";

export const params = new Proxy(new URLSearchParams(window.location.search), {
    // @ts-expect-error
    get: (searchParams, prop) => searchParams.get(prop),
}) as unknown as QueryParams;

export function initRun(runFn: (s: string, n: number, spineName: string) => void) {
    document.addEventListener("DOMContentLoaded", function() {
        runFn(params.spineV, parseInt(params.animNum), params.spineName || "flower");
    });
}
