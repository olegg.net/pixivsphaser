import { Application, Sprite } from "pixi.js";
import { Spine } from "pixi-spine";
import { bgFile, initRun, params } from "src/query";
import "./style.css";

initRun(run);

function run(spineV: string, animNum: number, spineName = "flower") {
    const app = new Application({
        backgroundColor: 0xd3d3d3,
        width: parseInt(params.width),
        height: parseInt(params.height),
    });

    const hawaii = Sprite.from(`./assets/${bgFile}.png`);

    hawaii.x = 0;
    hawaii.y = 0;

    app.stage.addChild(hawaii);

    const hawaii2 = Sprite.from(`./assets/${bgFile}.png`);

    hawaii2.scale.y = -1;
    hawaii2.x = 0;
    hawaii2.y = 1200;

    app.stage.addChild(hawaii2);

    app.loader.add("spineCharacter", `./assets/${spineV}/${spineName}.json`)
        .load(function(_loader, resources) {
            // @ts-expect-error
            const animation = new Spine(resources.spineCharacter.spineData);

            animation.x = 400;
            animation.y = 300;

            // add the animation to the scene and render...
            app.stage.addChild(animation);

            if (animation.state.hasAnimation(`flower_nearmiss_${animNum}`)) {
                // run forever, little boy!
                animation.state.setAnimation(0, `flower_nearmiss_${animNum}`, true);
            }

            app.start();
        });

    document.body.appendChild(app.view);
}
