/// <reference path="../node_modules/phaser/types/phaser.d.ts" />
/// <reference path="../node_modules/phaser/types/SpineGameObject.d.ts" />
/// <reference path="../node_modules/phaser/types/SpinePlugin.d.ts" />
/// <reference path="../node_modules/phaser/types/spine.d.ts" />

interface Window {
    SpinePlugin: SpinePlugin;
}

type QueryParams = {
    spineV: string;
    spineName?: string;
    animNum: string;
    width: string;
    height: string;
};
